## Before Opening an Issue

- **Avoid duplicates:** Please take a moment to search if the issue already exists.
- **Split up issues:** If you have multiple requests or are experiencing multiple bugs, please create separate issues for each. That way, I can address the issues individually.
- **Mark bugs as [Bug]:** It saves time if I can recognize important issues right away. Other issues don't have to be marked specifically, but it is nice.

...and delete this text of course.